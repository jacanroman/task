import React, {Component} from 'react';
import Councillors from './components/councillors';
import axios from 'axios';

    class App extends Component {

      state= {
        contacts:[]
      }



      componentDidMount(){
        axios
          .get('http://ws-old.parlament.ch/councillors?format=json')
        //fetch('http://jsonplaceholder.typicode.com/users')
        .then(res=>{
          console.log(res.data)
          this.setState({contacts: res.data})
        })
        .catch(err => console.warn(err))
        
      }
        
    /*
      componentDidMount(){
        fetch('http://ws-old.parlament.ch/councillors?format=json',{
          method: 'GET',
          headers:{
            'Content-Type': 'application/json'
          }
        })
        //fetch('http://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
        .then((data)=>{
          this.setState({contacts: data})
        })
        .catch(console.log)
        
      }

      
*/
      render () {
        return (
          <div className="card">
            <Councillors contacts={this.state.contacts} />
        </div>
        );
      }
    }

    export default App;
