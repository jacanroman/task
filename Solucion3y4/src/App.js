import React, {useState, useEffect} from 'react';
import axios from 'axios';

/*
import Cards from './components/Cards/Cards';
import Chart from './components/Chart/Chart';
import CountryPicker from './components/CountryPicker/CountryPicker';
*/

import { Cards, Chart, CountryPicker} from './components/index'
import styles from './App.module.css';
import {fetchData} from './api/index';

function App() {

    /*
    const [data, setData] = useState(fetchData)
    console.log(data)
    */

    //const url = http://ws-old.parlament.ch/councillors?format=json'
    /* I have a problem when and I couldn't fix when I tried to get the data from the API I have an error with the CORS I tried to fixed But I couldn't fixed it*/
    /* this is the error 
    Access to XMLHttpRequest at 'http://ws-old.parlament.ch/councillors?format=json' from origin 'http://localhost:3002' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource.*/

    const url = 'https://covid19.mathdro.id/api';
  
    const [tasks, setTasks] = useState([])
    
    useEffect(()=>{
      axios.get(url)
      .then(response=>{
        setTasks(response.data)
      })
      .catch(err=>{
        console.log("error",err)
      });
    },[])

    console.log(tasks)

  

  return (
    <div className={styles.prueba}>
      
      <h1>App</h1>
      <Cards data={tasks} />
      <CountryPicker />
      <Chart />
    </div>
  );
}

export default App;
