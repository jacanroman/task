
async function getCouncillors() {
  let url = 'http://ws-old.parlament.ch/councillors?format=json';
  let response = await fetch(url,{
    method: 'GET',
    headers: {
      'Content-type': 'application/json',
    }
  });
  let data = await response.json()
  return data
}

getCouncillors()

/*
const app = document.getElementById('root')


const container = document.createElement('div')
container.setAttribute('class', 'container')


app.appendChild(container)

let request = new XMLHttpRequest()
request.open('GET', 'http://ws-old.parlament.ch/councillors/basicdetails', true)
request.onload = function () {
  // Begin accessing JSON data here
  var data = JSON.parse(this.response)
  if (request.status >= 200 && request.status < 400) {
    data.forEach((movie) => {
      const card = document.createElement('div')
      card.setAttribute('class', 'card')

      const h1 = document.createElement('h1')
      h1.textContent = movie.title

      const p = document.createElement('p')
      movie.description = movie.description.substring(0, 300)
      p.textContent = `${movie.description}...`

      container.appendChild(card)
      card.appendChild(h1)
      card.appendChild(p)
    })
  } else {
    const errorMessage = document.createElement('marquee')
    errorMessage.textContent = `Gah, it's not working!`
    app.appendChild(errorMessage)
  }
}

request.send()
*/